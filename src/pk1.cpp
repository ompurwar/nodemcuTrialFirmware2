#include "ESPAsyncTCP.h"
#include "SyncClient.h"
#include <ArduinoJson.h>
#include <ArduinoOTA.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <WiFiClientSecure.h>
#include <WiFiUdp.h>
#include <string>

using namespace std;
void pollServer(String type);
static AsyncClient *aClient = NULL;

const char *ssid = "MyASUS1";
const char *password = "ultron123";

int prev_input_st[13] = {};
int state_array[13] = {};
const int pin[13] = {16, 5, 4, 0, 2, 14, 12, 13, 15, 3, 1, 9, 10};
bool state[5] = {true, true, true, true, true};
bool flag_update_state = false;
bool authenticate_flag = false;
bool send_data = true;
String SessionId = "dNRnzrSQz5";
long int counter = 0;

String getCredentials() {
  return String("{\"Id\":\"X123\",\"Key\": \"ZXCVBNM\"}");
};

int invert(int val) {
  if (val == 1) {
    // code
    return 0;
  } else if (val == 0) {
    return 1;
  } else {
    return -1;
  }
};

bool intToBool(int a) {
  if (a == 1) {
    return true;
  }
  if (a == 0) {
    return false;
  }
};

int boolToInt(bool a) {
  if (a == true) {
    return 1;
  }
  if (a == false) {
    return 0;
  }
};

void setup() {
  Serial.begin(9600);
  pinMode(pin[0], OUTPUT);       // indicator pin
  pinMode(pin[1], OUTPUT);       // relay 1 driver pin
  pinMode(pin[2], OUTPUT);       // relay 2 driver pin
  pinMode(pin[5], INPUT_PULLUP); // input form switch 1
  pinMode(pin[6], INPUT_PULLUP); // input from switch 2
  digitalWrite(pin[0], LOW);
  Serial.printf("switch state evalutation 1:\t%d\t%d\n", digitalRead(pin[5]),
                digitalRead(pin[6]));
  prev_input_st[0] = digitalRead(pin[5]);
  prev_input_st[1] = digitalRead(pin[6]);
  Serial.printf("switch state evalutation 2:\t%d\t%d\n", digitalRead(pin[5]),
                digitalRead(pin[6]));

  // attachInterrupt(digitalPinToInterrupt(pin[6]), toggle1, CHANGE);
  // attachInterrupt(digitalPinToInterrupt(pin[7]), toggle2, CHANGE);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    // Serial.println("Connection Failed! Rebooting...");
    Serial.println("connecting..");
    delay(1000);
  }

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

// Asynch TCP configuration
void runAsyncClientpollserver(String type) {
  String json;
  String path;
  String host = "hello-pc";
  int port = 8050;

  if (type == "updateState") {
    if (flag_update_state) {
      json = String(
          "{\"SessionId\":\"" + SessionId +
          "\",\"errFlag\": \"0\",\"state_array\":[{\"id\": 1,\"state\": " +
          String(invert(boolToInt(state[0]))) + "},{\"id\": 2,\"state\": " +
          String(invert(boolToInt(state[1]))) + "}]}");
      flag_update_state = false;
      Serial.println("updating " + json);
    } else {
      json = String("{\"SessionId\":\"" + SessionId + "\",\"errFlag\": \"0\"}");
      Serial.println("just synching " + json);
    }

    path = String("/syncstate");
  } else if (type == "authenticate") {
    json = getCredentials();
    path = String("/authenticate");
    Serial.print("authenticating..");
  }

  if (WiFi.status() == WL_CONNECTED) {
    if (aClient) {
      Serial.print("already connected");
    } // client already exists

    aClient = new AsyncClient();
    while (!aClient) {
      Serial.print("could not allocate client");
      aClient->connect(host.c_str(), port);
    } // could not allocate client

    aClient->onError(
        [](void *arg, AsyncClient *client, int error) {
          Serial.println("Connect Error");
          send_data = true;
          aClient = NULL;
          delete client;
        },
        NULL);

    aClient->onConnect(
        [host, port, path, json](void *arg, AsyncClient *client) {
          Serial.println("Connected");
          aClient->onError(NULL, NULL);

          client->onDisconnect(
              [](void *arg, AsyncClient *c) {
                Serial.println("Disconnected");
                send_data = true;
                aClient = NULL;
                delete c;
              },
              NULL);

          client->onData(
              [](void *arg, AsyncClient *c, void *data, size_t len) {
                send_data = true;
                Serial.print("\r\nData: ");
                Serial.println(len);
                String payload = String((char *)data);
                // uint8_t *d = (uint8_t *)data;
                payload =
                    payload.substring(payload.lastIndexOf("\r\n\r\n"), len);
                Serial.print(payload);
                StaticJsonBuffer<250> jsonBuffer;

                JsonObject &root = jsonBuffer.parseObject(payload);

                int errFlag = root["databox"]["errFlag"];
                const char *ResponseCode = root["databox"]["ResponseCode"];
                const char *SessId = root["databox"]["SessionID"];
                state_array[0] =
                    root["databox"]["state_array"][0]["state"]; // 1
                state_array[1] =
                    root["databox"]["state_array"][1]["state"]; // 0

                if (errFlag == 402) {
                  /* code */
                  authenticate_flag = true;
                  Serial.println("setting authentication flag to true");
                }

                if (errFlag == 0 && String(ResponseCode) != "200") {
                  /* code */
                  Serial.println("driving relays : " + String(state_array[0]) +
                                 " " + String(state_array[1]));
                  if (invert(intToBool(state_array[0])) != state[0]) {
                    state[0] = intToBool(invert(state_array[0]));
                  }
                  if (invert(intToBool(state_array[1])) != state[1]) {
                    state[1] = intToBool(invert(state_array[1]));
                  }
                }

                if (String(ResponseCode) == "200") {
                  /* code */
                  SessionId = String(SessId);
                  Serial.println("setting authentication flag to false");
                  Serial.println(SessionId);
                  authenticate_flag = false;
                }
                // for (size_t i = 0; i < len; i++)
                // Serial.write(d[i]);
              },
              NULL);

          String XYZ =
              "POST /api" + path + "/ HTTP/1.1" + "\r\n" + "Host:" + host +
              ":" + String(port) + "\r\n" + "Content-Type: application/json" +
              "\r\n" + "Cache-Control: no-cache" + "\r\n" +
              "accept-encoding: gzip, deflate" + "\r\n" + "content-length:" +
              String(json.length()) + "\r\n" + "Connection: close" + "\r\n\r\n";

          // Serial.write(XYZ+json);
          String tcppacket = XYZ + json + "\r\n";
          tcppacket.indexOf("\r\n\r\n");
          char *data = new char[tcppacket.length() + 1];
          strcpy(data, tcppacket.c_str());
          client->write(data);
          // Serial.println(tcppacket);
        },
        NULL);

    if (!aClient->connect(host.c_str(), port)) {
      Serial.println("Connect Fail");
      AsyncClient *client = aClient;
      aClient = NULL;
      delete client;
    }
  }
};

void loop() {
  int gpio_read[10];
  counter++;
  gpio_read[0] = digitalRead(pin[5]);
  gpio_read[1] = digitalRead(pin[6]);

  if (gpio_read[0] != prev_input_st[0]) {
    prev_input_st[0] = gpio_read[0];
    if (gpio_read[0] != boolToInt(state[0])) {
      state[0] = intToBool(gpio_read[0]);
      flag_update_state = true;
    }
  }
  if (gpio_read[1] != prev_input_st[1]) {
    prev_input_st[1] = gpio_read[1];
    if (gpio_read[1] != boolToInt(state[1])) {
      state[1] = intToBool(gpio_read[1]);
      flag_update_state = true;
    }
  }
  digitalWrite(pin[1], state[0]);
  digitalWrite(pin[2], state[1]);
  //Serial.print(String(counter)+"\n");

  if (counter >= 1000) {

    if (send_data) {
      if (authenticate_flag) {
        runAsyncClientpollserver(String("authenticate"));
      } else {
        runAsyncClientpollserver(String("updateState"));
        Serial.print("hi om what are you doing?");
      }
      send_data = false;
    }
    counter = 0;
  }
  delayMicroseconds(1000);
}