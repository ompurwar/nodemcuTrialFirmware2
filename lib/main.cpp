//on collaboration wit prabhakar singh

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>

#include <ArduinoOTA.h>
#include "ESPAsyncTCP.h"
#include "SyncClient.h"

#include <FS.h>
#include <WiFiClientSecure.h>

#include <WiFiUdp.h>
//#include "ajson.h"
#include <ArduinoJson.h>
//#include <ArduinoOTA.h>

const int pin[13] = {16, 5, 4, 0, 2, 14, 12, 13, 15, 3, 1, 9, 10};
int a = 0, b = 0;

const char *ssid = "MyASUS1";
const char *password = "ultron123";
int prev_input_st[13] = {};
int state_array[13] = {};
bool flag_update_state = false;
bool authenticate_flag = false;

String SessionId;
long int counter = 0;
bool state[5] = {true, true, true, true, true};

void pollServer(String type);




String getCredentials()
{
  return String("{\"Id\":\"X123\",\"Key\": \"ZXCVBNM\"}");
};






void SwithcToAutomode()
{
  /* code */
  pinMode(pin[0], OUTPUT);
  pinMode(pin[1], OUTPUT);
  pinMode(pin[2], OUTPUT);
  pinMode(pin[3], OUTPUT);
  pinMode(pin[4], OUTPUT);
  pinMode(pin[5], OUTPUT);
  pinMode(pin[6], OUTPUT);
  pinMode(pin[7], OUTPUT);
  pinMode(pin[8], OUTPUT);
  //pinMode(pin[9], OUTPUT);
  //pinMode(pin[10], OUTPUT);
  //pinMode(pin[11], OUTPUT);
  //pinMode(pin[12], OUTPUT);
}



int invert(int val)
{
  if (val == 1)
  {
    // code
    return 0;
  }
  else if (val == 0)
  {
    return 1;
  }
  else
  {
    return -1;
  }
};



bool intToBool(int a)
{
  if (a == 1)
  {
    return true;
  }
  if (a == 0)
  {
    return false;
  }
};




/*void toggle1(){
  delayMicroseconds(10000);
  state[0]=intToBool(digitalRead(pin[6]));
  flag_update_state=true;
  Serial.println("Switch1toggle");
};

void toggle2(){
  delayMicroseconds(10000);
  state[1]=intToBool(digitalRead(pin[7]));
  flag_update_state=true;
  Serial.println("Switch2toggle");
};
*/




int boolToInt(bool a)
{
  if (a == true)
  {
    return 1;
  }
  if (a == false)
  {
    return 0;
  }
}




void setup()
{
  Serial.begin(9600);
  pinMode(pin[0],OUTPUT);   //indicator pin
  pinMode(pin[1], OUTPUT);  //relay 1 driver pin
  pinMode(pin[2], OUTPUT);  //relay 2 driver pin
  pinMode(pin[5], INPUT_PULLUP);  //input form switch 1
  pinMode(pin[6], INPUT_PULLUP);  //input from switch 2
  digitalWrite(pin[0],LOW);
  Serial.printf("switch state evalutation 1:\t%d\t%d\n", digitalRead(pin[5]), digitalRead(pin[6]));
  prev_input_st[0] = digitalRead(pin[5]);
  prev_input_st[1] = digitalRead(pin[6]);
  Serial.printf("switch state evalutation 2:\t%d\t%d\n", digitalRead(pin[5]), digitalRead(pin[6]));

  //attachInterrupt(digitalPinToInterrupt(pin[6]), toggle1, CHANGE);
  //attachInterrupt(digitalPinToInterrupt(pin[7]), toggle2, CHANGE);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    //Serial.println("Connection Failed! Rebooting...");
    Serial.println("connecting..");
    delay(1000);
  }

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop()
{
  int gpio_read[10];
  counter++;

  gpio_read[0] = digitalRead(pin[5]);
  gpio_read[1] = digitalRead(pin[6]);

  if (gpio_read[0] != prev_input_st[0])
  {
    prev_input_st[0] = gpio_read[0];
    if (gpio_read[0] != boolToInt(state[0]))
    {
      state[0] = intToBool(gpio_read[0]);
      flag_update_state = true;
    }
  }
  if (gpio_read[1] != prev_input_st[1])
  {
    prev_input_st[1] = gpio_read[1];
    if (gpio_read[1] != boolToInt(state[1]))
    {
      state[1] = intToBool(gpio_read[1]);
      flag_update_state = true;
    }
  }
  digitalWrite(pin[1], state[0]);
  digitalWrite(pin[2], state[1]);

  if (counter >= 1000)
  {

    if (authenticate_flag)
    {

      pollServer(String("authenticate"));
    }
    else
    {
      pollServer(String("updateState"));
    }
    counter = 0;
  }
  delayMicroseconds(1000);
  /*delay(1);
  delayMicroseconds(100);
  int val ;
  val= analogRead(A0);
  delayMicroseconds(10000);
  //Serial.println(val);    // read the input pin


  /*if(abs(val-analogRead(A0))>12){
    pinMode(pin[0], INPUT);
    pinMode(pin[1], INPUT);
    digitalWrite(pin[0],LOW);
    digitalWrite(pin[1],LOW);
    a=digitalRead(pin[0]);
    b=digitalRead(pin[1]);
    pinMode(pin[0], OUTPUT);
    pinMode(pin[1], OUTPUT);
  }
  Serial.printf("\tvalue read\t:");
  Serial.printf(" %d %d \n",a,b);
  Serial.printf("\tcounter value is\t:%d\t difference value\t:%d\t%d\t%d",counter,abs(analogRead(A0)),val,abs(val-analogRead(A0)));
*/

  /*pinMode(pin[0], INPUT);
  pinMode(pin[1], INPUT);
  digitalWrite(pin[0],LOW);
  digitalWrite(pin[1],LOW);
  Serial.printf("value read n");
  Serial.printf(" %d %d \n",digitalRead(pin[0]),digitalRead(pin[1]));
  pinMode(pin[0], OUTPUT);
  pinMode(pin[1], OUTPUT);*/
}

void pollServer(String type)
{
  String json;
  String path;
  String host;

  if (type == "updateState")
  {
    if (flag_update_state)
    {
      json = String("{\"SessionId\":\"" + SessionId + "\",\"errFlag\": \"0\",\"state_array\":[{\"id\": 1,\"state\": " + String(invert(boolToInt(state[0]))) + "},{\"id\": 2,\"state\": " + String(invert(boolToInt(state[1]))) + "}]}");
      flag_update_state = false;
      Serial.println("updating " + json);
    }
    else
    {
      json = String("{\"SessionId\":\"" + SessionId + "\",\"errFlag\": \"0\"}");
    }

    path = String("/syncstate");
  }
  else if (type == "authenticate")
  {
    json = getCredentials();
    path = String("/authenticate");
    Serial.print("authenticating..");
  }

  if (WiFi.status() == WL_CONNECTED)
  { //checking wifi status
    /* code*/
    HTTPClient http;
    //http.begin("http://postest-env.ap-south-1.elasticbeanstalk.com/api"+path);
    host = "http://hello-pc:8050";
    //host = "http://postest-env.ap-south-1.elasticbeanstalk.com";
    String destinedServer = host + "/api" + path;
    //Serial.print(destinedServer);
    http.begin(destinedServer);

    http.addHeader("Content-Type", "application/json");
    http.addHeader("Content-Length", String(json.length()));
    int httpCode = http.POST(json);
    Serial.println(json + "\n");

    if (httpCode > 0)
    {                                    //Check the returning code
      String payload = http.getString(); //Get the request response payload
      Serial.println(payload);             //Print the response payload
      StaticJsonBuffer<250> jsonBuffer;

      JsonObject &root = jsonBuffer.parseObject(payload);

      int errFlag = root["databox"]["errFlag"];
      const char *ResponseCode = root["databox"]["ResponseCode"];
      const char *SessId = root["databox"]["SessionID"];
      state_array[0] = root["databox"]["state_array"][0]["state"]; // 1
      state_array[1] = root["databox"]["state_array"][1]["state"]; // 0

      if (errFlag == 402)
      {
        /* code */
        authenticate_flag = true;
        Serial.println("setting authentication flag to true");
      }

      if (errFlag == 0 && String(ResponseCode) != "200")
      {
        /* code */
        Serial.println("driving relays : " + String(state_array[0]) + " " + String(state_array[1]));
        if (invert(intToBool(state_array[0])) != state[0])
        {
          state[0] = intToBool(invert(state_array[0]));
        }
        if (invert(intToBool(state_array[1])) != state[1])
        {
          state[1] = intToBool(invert(state_array[1]));
        }
      }

      if (String(ResponseCode) == "200")
      {
        /* code */
        SessionId = String(SessId);
        Serial.println("setting authentication flag to false");
        Serial.println(SessionId);
        authenticate_flag = false;
      }
    }
    http.end(); //Close connection root["databox"]["SessionID"]
  }
  //[[ print("sending data!",buffer)  ]]
};
