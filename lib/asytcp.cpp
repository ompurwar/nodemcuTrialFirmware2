#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoOTA.h>
#include "ESPAsyncTCP.h"
#include "SyncClient.h"
#include <FS.h>
#include <WiFiClientSecure.h>
#include <WiFiUdp.h>
//#include "ajson.h"
#include <ArduinoJson.h>
//#include <ArduinoOTA.h>

static AsyncClient * aClient = NULL;

const char *ssid = "MyASUS1";
const char *password = "ultron123";
int prev_input_st[13] = {};
int state_array[13] = {};
bool flag_update_state = false;
bool authenticate_flag = false;

String SessionId;
long int counter = 0;

void pollServer(String type);

String getCredentials()
{
  return String("{\"Id\":\"X123\",\"Key\": \"ZXCVBNM\"}");
};
bool state[5] = {true, true, true, true, true};
void setup()
{
Serial.begin(9600);
Serial.println("Booting");
WiFi.mode(WIFI_STA);
WiFi.begin(ssid, password);

while (WiFi.waitForConnectResult() != WL_CONNECTED)
{
  //Serial.println("Connection Failed! Rebooting...");
  Serial.println("connecting..");
  delay(1000);
}

Serial.println("Ready");
Serial.print("IP address: ");
Serial.println(WiFi.localIP());
};


//Asynch TCP configuration
void runAsyncClientpollserver(String type)
{
  if(aClient)//client already exists
    return;

  aClient = new AsyncClient();
  if(!aClient)//could not allocate client
    return;

  aClient->onError([](void * arg, AsyncClient * client, int error)
  {
    Serial.println("Connect Error");
    aClient = NULL;
    delete client;
  }, NULL);

  aClient->onConnect([](void * arg, AsyncClient * client)
  {


    Serial.println("Connected");
    aClient->onError(NULL, NULL);

    client->onDisconnect([](void * arg, AsyncClient * c)
    {
      Serial.println("Disconnected");
      aClient = NULL;
      delete c;
    }, NULL);

    client->onData([](void * arg, AsyncClient * c, void * data, size_t len){
      Serial.print("\r\nData: ");
      Serial.println(len);
      uint8_t * d = (uint8_t*)data;
      for(size_t i=0; i<len;i++)
        Serial.write(d[i]);
      }, NULL);


      //send the request
      client->write("POST /api/syncstate/ HTTP/1.1\r\nHost: hello-pc:8050\r\nContent-Type: application/json\r\nCache-Control: no-cache\r\naccept-encoding: gzip, deflate\r\ncontent-length: 100\r\nConnection: close\r\n\r\n{\"SessionId\":\"dNRnzrSQz5\",\"errFlag\":\"0\",\"state_array\": [{\"id\": 1,\"state\": 1},{\"id\": 2,\"state\": 1}]}\r\n");
      //client->write("GET /api/?data={\"Hello\":\"World\"} HTTP/1.1\r\nHost: fake-response.appspot.com\r\n\r\n");
      Serial.print("POST /api/syncstate/ HTTP/1.1\r\nHost: hello-pc:8050\r\nContent-Type: application/json\r\nCache-Control: no-cache\r\n\r\naccept-encoding: gzip, deflate\r\ncontent-length: 100\r\nConnection: close\r\n\r\n{\"SessionId\":\"dNRnzrSQz5\",\"errFlag\":\"0\",\"state_array\": [{\"id\": 1,\"state\": 1},{\"id\": 2,\"state\": 1}]}\r\n");
    }, NULL);

    if(!aClient->connect("hello-pc", 8050))
    {
      Serial.println("Connect Fail");
      AsyncClient * client = aClient;
      aClient = NULL;
      delete client;
    }
  };

void loop()
{
runAsyncClientpollserver();
delay(10000);
};
